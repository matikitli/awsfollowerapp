package com.matikitli.awsfollower.model;

public enum LogEntryType {
    ERROR,
    WARN,
    INFO;
}
