package com.matikitli.awsfollower.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Set;

@Data
public abstract class LogEntryBase implements LogEntry {

    private Integer id;

    private LogEntryType type;

    @DateTimeFormat(pattern = "HH:mm:ss.SSS yy/MM/dd")
    private LocalDate date;

    private String message;

    private Set<String> tags;

    private String getShortMessage() {
        return this.message.substring(0,15);
    }
}
