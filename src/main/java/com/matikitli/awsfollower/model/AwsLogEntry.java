package com.matikitli.awsfollower.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AwsLogEntry extends LogEntryBase {

    private String serviceName;

}
