package com.matikitli.awsfollower.service;

import com.matikitli.awsfollower.model.LogEntry;
import org.springframework.stereotype.Service;

import java.util.List;

public interface LogService<T extends LogEntry> {

    public List<T> getAllLogs();

    public T getLogById(Integer id);

}
