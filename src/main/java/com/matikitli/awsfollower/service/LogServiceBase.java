package com.matikitli.awsfollower.service;

import com.matikitli.awsfollower.model.LogEntry;

import java.util.Collections;
import java.util.List;

public abstract class LogServiceBase<T extends LogEntry> implements LogService {

    @Override
    public List<T> getAllLogs() {
        return Collections.emptyList();
    }

    @Override
    public T getLogById(Integer id) {
        return null;
    }
}
