package com.matikitli.awsfollower.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class LogsCommand {

    @Autowired
    private ConsoleService consoleService;

    @ShellMethod("Add two int")
    public void add(int a, int b){
        int result=a+b;
        consoleService.write("Added: "+result);
    }
}
