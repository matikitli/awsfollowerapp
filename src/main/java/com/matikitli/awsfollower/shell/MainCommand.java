package com.matikitli.awsfollower.shell;

import com.matikitli.awsfollower.service.AwsLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class MainCommand {

    private final ConsoleService console;

    private final AwsLogService awsLogService;

    @Autowired
    public MainCommand(ConsoleService console, AwsLogService awsLogService)
    {
        this.console = console;
        this.awsLogService = awsLogService;
    }

    @ShellMethod("Say hello to person")
    public void sayHi(String person) {
        this.console.write("Hello "+person+"!");
    }
}
