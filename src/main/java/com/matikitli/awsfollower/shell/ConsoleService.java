package com.matikitli.awsfollower.shell;

import org.springframework.stereotype.Service;

import java.io.PrintStream;

@Service
public class ConsoleService {

    private static final PrintStream out = System.out;
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET_PREV = "\u001B[0m";


    public void write(String msg, String... args) {
        this.out.print("> ");
        this.out.print(ANSI_YELLOW);
        this.out.printf(msg, (Object[]) args);
        this.out.print(ANSI_RESET_PREV);
        this.out.println();
    }
}
