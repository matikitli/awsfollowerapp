package com.matikitli.awsfollower;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsfollowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsfollowerApplication.class, args);
	}
}
